package com.soldatenko.wavetest;

import java.awt.*;
import java.util.Arrays;
import java.util.LinkedList;

public abstract class Controller {

    private final LinkedList<Color> colors = new LinkedList<Color>(Arrays.asList(
            Color.BLUE,
            Color.GREEN,
            Color.RED,
            Color.CYAN,
            Color.ORANGE,
            Color.DARK_GRAY,
            Color.PINK,
            Color.YELLOW,
            Color.MAGENTA));

    private Color getNextColor() {
        Color color = colors.poll();
        colors.add(color);
        return color;
    }

    public void startWave(int y) {
        getModel().setWave(y, getNextColor());
    }

    public void shiftRight() {
        getModel().shiftRight();
    }

    public abstract WaveTestModel getModel();
}
