package com.soldatenko.wavetest;

import java.awt.*;
import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArrayList;

public class WaveTestModel {

    private final int rows;
    private final int cols;
    private CopyOnWriteArrayList<ModelListener> listeners = new CopyOnWriteArrayList<ModelListener>();
    private final int[] bitmap;

    public WaveTestModel(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        // rows * cols - visible part
        // rows * rows - invisible square box on the left
        bitmap = new int[rows * rows + rows * cols];
        Arrays.fill(bitmap, Color.LIGHT_GRAY.getRGB());
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    protected void fireImageChanged() {
        for (ModelListener listener : listeners) {
            listener.onImageChanged();
        }
    }

    public void addModelListener(ModelListener listener) {
        listeners.add(listener);
    }

    public void removeModelListener(ModelListener listener) {
        listeners.remove(listener);
    }

    public void shiftRight() {
        System.arraycopy(bitmap, 0, bitmap, rows, bitmap.length - rows);
        fireImageChanged();
    }

    public void setWave(int waveY, Color color) {
        int invisibleCols = rows;
        int waveX = (invisibleCols - 1);
        for (int x = 0; x < invisibleCols; x++) {
            for (int y = 0; y < rows; y++) {
                boolean isInWave = Math.abs(waveX - x) >= Math.abs(waveY - y);
                if (isInWave) {
                    bitmap[x * rows + y] = color.getRGB();
                }
            }
        }
        fireImageChanged();
    }

    public Color getCell(int x, int y) {
        int invisibleCols = rows;
        return new Color(bitmap[rows * (x + invisibleCols) + y]);
    }
}
