package com.soldatenko.wavetest;

public interface ModelListener {
    void onImageChanged();
}
