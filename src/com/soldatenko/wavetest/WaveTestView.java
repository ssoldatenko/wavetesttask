package com.soldatenko.wavetest;

import javax.swing.*;
import java.awt.*;

public class WaveTestView extends JPanel implements ModelListener {

    private WaveTestModel model;
    private final int pixelPerCell;

    public WaveTestView(WaveTestModel model, int pixelPerCell) {
        this.model = model;
        this.pixelPerCell = pixelPerCell;
    }

    public void setModel(WaveTestModel newModel) {
        if (model != null) {
            model.removeModelListener(this);
        }
        model = newModel;
        if (model != null) {
            model.addModelListener(this);
        }
    }

    @Override
    public void onImageChanged() {
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        if (model == null) {
            return;
        }
        g.setColor(Color.WHITE);
        g.drawRect(0, 0, getWidth(), getHeight());
        Color oldColor = g.getColor();
        for (int x = 0; x < model.getCols(); x++) {
            for (int y = 0; y < model.getRows(); y++) {
                Color cellColor = model.getCell(x, y);
                if (cellColor != oldColor) {
                    oldColor = cellColor;
                    g.setColor(cellColor);
                }
                g.fillRect(x * pixelPerCell, y * pixelPerCell, pixelPerCell, pixelPerCell);
            }
        }
    }

    public WaveTestModel getModel() {
        return model;
    }
}

