import com.soldatenko.wavetest.Controller;
import com.soldatenko.wavetest.WaveTestModel;
import com.soldatenko.wavetest.WaveTestView;

import javax.swing.*;
import java.awt.event.*;

public class WaveTestApp {

    private int initialWidth = 300;
    private int initialHeight = 200;
    private int timeout = 100;
    private int pixelPerCell = 10;
    private JFrame frame;
    private WaveTestView view;
    private Controller controller;
    private Timer timer;

    public static void main(String[] args) {
        WaveTestApp app = new WaveTestApp();
        for (String arg : args) {
            if (arg.startsWith("-w")) {
                app.setInitialWidth(Integer.parseInt(arg.substring(2)));
            } else if (arg.startsWith("-h")) {
                app.setInitialHeight(Integer.parseInt(arg.substring(2)));
            } else if (arg.startsWith("-t")) {
                app.setTimeout(Integer.parseInt(arg.substring(2)));
            } else if (arg.startsWith("-p")) {
                app.setPixelPerCell(Integer.parseInt(arg.substring(2)));
            } else {
                showUsage();
                return;
            }
        }
        app.showFrame();
    }

    private static void showUsage() {
        System.out.println("WaveTestTask - application to display running waves.");
        System.out.println("Usage:");
        System.out.println("    java WaveTestApp");
        System.out.println("    or");
        System.out.println("    java WaveTestApp -w600 -h400 -t250 -p10");
        System.out.println("");
        System.out.println("Parameters:");
        System.out.println("    -w600 - 600 pixels wide");
        System.out.println("    -h400 - 400 pixels high");
        System.out.println("    -t250 - slide one cell every 250 milliseconds");
        System.out.println("    -p10 - draw every cell as 10x10 square");
        System.out.println("");
    }

    public void setInitialWidth(int width) {
        if (width <= 0) {
            throw new IllegalArgumentException("Width can not be negative or zero.");
        }
        initialWidth = width;
    }

    public void setInitialHeight(int height) {
        if (initialHeight <= 0) {
            throw new IllegalArgumentException("Height can not be negative or zero.");
        }
        initialHeight = height;
    }

    public void setTimeout(int timeout) {
        if (timeout <= 0) {
            throw new IllegalArgumentException("Timeout can not be negative or zero.");
        }
        this.timeout = timeout;
    }

    public void setPixelPerCell(int pixelPerCell) {
        if (pixelPerCell <= 0) {
            throw new IllegalArgumentException("Pixels per cell can not be negative or zero");
        }
        this.pixelPerCell = pixelPerCell;
    }

    public void showFrame() {
        controller = new Controller() {
            @Override
            public WaveTestModel getModel() {
                return view.getModel();
            }
        };
        WaveTestModel initialModel = new WaveTestModel(1, 1); // we don't know view size yet.
        view = new WaveTestView(initialModel, pixelPerCell);
        view.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int rowIndex = e.getY() / pixelPerCell;
                controller.startWave(rowIndex);
            }
        });
        timer = new Timer(timeout, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.shiftRight();
            }
        });

        frame = new JFrame();
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                adjustModel();
            }
        });
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(view);
        frame.setBounds(0, 0, initialWidth, initialHeight);
        frame.setVisible(true);
        timer.start();
    }

    private void adjustModel() {
        int rows = (int) Math.round(Math.ceil((double) view.getHeight() / pixelPerCell));
        int cols = (int) Math.round(Math.ceil((double) view.getWidth() / pixelPerCell));
        WaveTestModel model = view.getModel();
        if (model == null || model.getRows() != rows || model.getCols() != cols) {
            view.setModel(new WaveTestModel(rows, cols));
        }
    }
}
