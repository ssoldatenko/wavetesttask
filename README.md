# Wave Test App

This project is GUI (Swing) app to show running waves.

To compile:

    $ ant

Or manually compile:

    $ mkdir target
    $ javac -d target src/*.java src/com/soldatenko/wavetest/*.java

To run:

    $ java -cp target WaveTestApp

or

    $ java -cp target WaveTestApp --usage


# Design

See UML diagrams in `docs` folder.

This application have 2 actors - **User** and **Timer**.

User can replace model by resizing main window, and he can start new wave by clicking on view.

Timer can shift waves right by calling `shiftRight()` method.
